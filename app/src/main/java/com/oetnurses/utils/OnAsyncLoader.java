package com.oetnurses.utils;

import org.json.JSONException;

public interface OnAsyncLoader {

    void onResult(String result) throws JSONException;

    void onStart();

    void onStop();
}