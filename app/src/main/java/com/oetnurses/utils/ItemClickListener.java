package com.oetnurses.utils;

import android.view.View;

import com.oetnurses.model.ExamTipsModel;

public interface ItemClickListener {
    void onItemClick(View view, int position, boolean isFromGameBooster, ExamTipsModel mExamTipsModel);
}