package com.oetnurses.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.oetnurses.R;
import com.oetnurses.model.ResultScoreModel;

import java.util.List;

public class ResultScoreListAdapter extends RecyclerView.Adapter<ResultScoreListAdapter.MyViewHolder> {

    private List<ResultScoreModel> bookList;
    Context context;


    public ResultScoreListAdapter(List<ResultScoreModel> bookList, Context context) {
        this.bookList = bookList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.resultscorelayout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ResultScoreModel model = bookList.get(position);
        if(model != null){

            if(model.getTitle() != null && !model.getTitle().isEmpty() && model.getPercentage() != null && !model.getPercentage().isEmpty()){
                holder.title.setText(model.getTitle()+" "+model.getPercentage());
            }else if(model.getTitle() != null && !model.getTitle().isEmpty()){
                holder.title.setText(model.getTitle());
            }else{
                holder.title.setVisibility(View.GONE);
            }


            if(model.getTotalQuestion() != null && !model.getTotalQuestion().isEmpty()){
                holder.from.setText(model.getTotalQuestion());
            }else{
                holder.from.setText("-");
            }
            if(model.getCorrectAns() != null && !model.getCorrectAns().isEmpty()){
                holder.score.setText(model.getCorrectAns());
            }else{
                holder.score.setText("-");
            }

        }
    }


    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView score;
        TextView from;

        LinearLayout priceLayout;

        public MyViewHolder(View view) {
            super(view);

            this.setIsRecyclable(false);
            title = view.findViewById(R.id.txtScoreTitle);
            score = view.findViewById(R.id.txtScore);
            from = view.findViewById(R.id.txtFrom);
        }
    }

}


